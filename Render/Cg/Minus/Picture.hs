-- | Rendering of "Data.Cg.Minus.Picture".
module Render.Cg.Minus.Picture where

import qualified Graphics.Rendering.Cairo as C {- cairo -}
import qualified Graphics.Rendering.Cairo.Matrix as C (Matrix {- cairo -} (..))

import Data.Cg.Minus.Colour {- hcg-minus -}
import Data.Cg.Minus.Picture {- hcg-minus -}
import Data.Cg.Minus.Types {- hcg-minus -}

import qualified Render.Cg.Minus as Render {- hcg-minus-cairo -}

type R = Double

fill :: Ca -> C.Render ()
fill clr = Render.colour clr >> C.fill

stroke :: Pen R -> C.Render ()
stroke (Pen lw clr dash) = Render.pen lw clr dash >> C.stroke

mark :: Either (Pen R) Ca -> C.Render ()
mark = either stroke fill

mark_render :: Mark R -> C.Render ()
mark_render m =
  case m of
    Line pen (Ln p1 p2) -> Render.line2 p1 p2 >> stroke pen
    Polygon e pt -> Render.polygon (Ls pt) >> mark e
    Circle e (c, r) -> Render.circle c r >> mark e
    Dot clr (c, r) -> Render.circle c r >> fill clr

-- | m = margin
picture_render :: R -> Wn R -> Picture R -> C.Render ()
picture_render m w p = do
  let Wn (Pt x y) (Vc _ dy) = w
  C.translate ((m / 2) - x) (dy + (m / 2) + y)
  C.transform (C.Matrix 1 0 0 (-1) 0 0)
  mapM_ mark_render p

-- | 'Render.render_to_file' of 'picture_render'.
picture_to_file_set :: [Render.File_Type] -> R -> FilePath -> Picture R -> IO ()
picture_to_file_set ty_set m fn p = do
  let w = picture_wn p
      Wn _ (Vc x y) = w
  mapM_ (\ty -> Render.render_to_file ty (m + x, m + y) fn (picture_render m w p)) ty_set

picture_to_pdf :: R -> FilePath -> Picture R -> IO ()
picture_to_pdf = picture_to_file_set [Render.F_Pdf]

picture_to_svg :: R -> FilePath -> Picture R -> IO ()
picture_to_svg = picture_to_file_set [Render.F_Svg]

picture_to_pdf_and_svg :: R -> FilePath -> Picture R -> IO ()
picture_to_pdf_and_svg = picture_to_file_set [Render.F_Pdf, Render.F_Svg]
