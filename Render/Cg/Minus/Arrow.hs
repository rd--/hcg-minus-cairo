-- | Rendering of "Data.Cg.Minus.Arrow".
module Render.Cg.Minus.Arrow where

import qualified Graphics.Rendering.Cairo as Cairo {- cairo -}

import Data.Cg.Minus.Arrow {- hcg-minus -}
import Data.Cg.Minus.Colour {- hcg-minus -}
import Data.Cg.Minus.Core {- hcg-minus -}
import Data.Cg.Minus.Types {- hcg-minus -}

import Render.Cg.Minus

arrow_strk :: Ca -> Cairo.Render ()
arrow_strk c = do
  Cairo.setLineCap Cairo.LineCapRound
  pen 0.01 c ([], 0)
  Cairo.stroke

{- | Render 'Ln' with solid arrow tip at endpoint.  Arrow tip
co-ordinates are given by 'arrow_coord'.
-}
arrow_ep :: R -> R -> Ca -> Ln R -> Cairo.Render ()
arrow_ep n a c ln = do
  let Ln p0 p1 = ln
      (p2, p3) = arrow_coord ln n a
  line (Ls [p0, ln_midpoint (Ln p2 p3)])
  arrow_strk c
  polygon (Ls [p2, p1, p3])
  Cairo.fill

-- | Variant of 'arrow_ep' to render 'Ls' as sequence of arrows.
arrows_ep :: R -> R -> Ca -> Ls R -> Cairo.Render ()
arrows_ep n a c (Ls xs) = mapM_ (arrow_ep n a c) (zipWith Ln xs (tail xs))

-- | Variant of 'arrow_ep' with draws tip at mid-point of 'Ln'.
arrow_mp :: R -> R -> Ca -> Ln R -> Cairo.Render ()
arrow_mp n a c ln = do
  let Ln p0 p1 = ln
      p1' = ln_midpoint (Ln p0 (pt_linear_extension n ln))
      (p2, p3) = arrow_coord (Ln p0 p1') n a
  line (Ls [p0, p1])
  arrow_strk c
  polygon (Ls [p2, p1', p3])
  Cairo.fill

-- | Variant of 'arrow_mp' to render 'Ls' as sequence of arrows.
arrows_mp :: R -> R -> Ca -> Ls R -> Cairo.Render ()
arrows_mp n a c (Ls xs) = mapM_ (arrow_mp n a c) (zipWith Ln xs (tail xs))
