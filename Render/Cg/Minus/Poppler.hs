module Render.Cg.Minus.Poppler where

import System.Exit {- base -}
import System.FilePath {- filepath -}
import System.Process {- process -}

-- | (command-name,arguments)
type Cmd = (String, [String])

-- | 'rawSystem'.
run_cmd :: Cmd -> IO ExitCode
run_cmd = uncurry rawSystem

-- | 'callProcess'.
run_cmd_ :: Cmd -> IO ()
run_cmd_ = uncurry callProcess

-- | 'unwords' of ':'
cmd_to_str :: Cmd -> String
cmd_to_str (c, a) = unwords (c : a)

-- | pdftocairo command to translate a PDF file to an SVG file.
pdf_to_svg_cmd :: FilePath -> FilePath -> Cmd
pdf_to_svg_cmd f1 f2 = ("pdftocairo", ["-svg", f1, f2])

-- | Variant where output file name is the input file with a replaced extension.
pdf_to_svg_rename_cmd :: FilePath -> Cmd
pdf_to_svg_rename_cmd fn = pdf_to_svg_cmd fn (replaceExtension fn ".svg")

-- > pdf_to_svg_rename "/tmp/t.0.pdf"
pdf_to_svg_rename :: FilePath -> IO ()
pdf_to_svg_rename = run_cmd_ . pdf_to_svg_rename_cmd
