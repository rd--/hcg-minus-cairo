# csv-to-svg

The following Csv files can be drawn:

- pt = point data (x,y) : (real,real)
- ln = line data (x1,y1,x2,y2) : (real,real,real,real)
- ls = line segment data (segment-id,group-id,x,y) : (int,int,real,real)

Options are to invert the y-axis (t = true, f = false) and the line-width or square-size.

~~~~
$ hcg-minus-csv-to-svg ln f 0.2 ~/sw/hcg-minus-cairo/data/csv/SQ.21.112.csv /tmp/SQ.21.112.svg
$ rsvg-view-3 /tmp/SQ.21.112.svg
~~~~
