all:
	echo "hcg-minus-cairo"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -Rf dist dist-newstyle *~
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh hcg-minus-cairo

push-tags:
	r.gitlab-push.sh hcg-minus-cairo --tags

indent:
	fourmolu -i Render

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Render
