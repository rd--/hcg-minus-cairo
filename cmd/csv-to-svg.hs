import Control.Monad {- base -}
import Data.Function {- base -}
import Data.List {- base -}
import System.Environment {- base -}
import System.FilePath {- filepath -}

import qualified Graphics.Rendering.Cairo as C {- cairo -}
import qualified Graphics.Rendering.Cairo.Matrix as C (Matrix(..)) {- cairo -}

import qualified Music.Theory.Array.Csv as T {- hmt-base -}

import Data.Cg.Minus.Colour {- hcg-minus -}
import Data.Cg.Minus.Core {- hcg-minus -}
import Data.Cg.Minus.Types {- hcg-minus -}

import qualified Render.Cg.Minus as Render {- hcg-minus-cairo -}

-- * Csv

type R = Double

-- | Load Csv file with columns (x,y).
load_pt_data :: FilePath -> IO [Pt R]
load_pt_data fn = do
  d <- T.csv_table_read_def read fn
  let f (x:y:_) = Pt x y
      f _ = error "load_pt_data"
  return (map f d)

-- | Load Csv file with columns (x1,y1,x2,y2).
load_ln_data :: FilePath -> IO [Ln R]
load_ln_data fn = do
  d <- T.csv_table_read_def read fn
  let f (x0:y0:x1:y1:_) = Ln (Pt x0 y0) (Pt x1 y1)
      f _ = error "load_ln_data"
  return (map f d)

-- | Load Csv file with columns (segment-id,group-id,x,y).
-- Equal segment-id implies equal group-id.
load_ls_data_raw :: FilePath -> IO [(Int,Int,R,R)]
load_ls_data_raw fn = do
  d <- T.csv_table_read_def id fn
  let f (uid:gid:x:y:_) = (read uid,read gid,read x,read y)
      f _ = error "load_ls_data"
  return (map f d)

-- | Transform into (group-id,line-segment).
--
-- > ls_data_delinearise [(0,10,0,0),(0,10,0.5,0.5),(0,10,1,1),(1,10,1,1),(1,10,0,0)]
ls_data_delinearise :: [(Int,Int,R,R)] -> [(Int,Ls R)]
ls_data_delinearise =
    let f = (==) `on` (\(x,_,_,_) -> x)
        g (_,k,x,y) = (k,Pt x y)
        h sq@((k,_):_) = (k,Ls (map snd sq))
        h _ = error "ls_data_delinearise"
    in map (h . map g) . groupBy f

load_ls_data :: FilePath -> IO [(Int,Ls R)]
load_ls_data = fmap ls_data_delinearise . load_ls_data_raw

-- * Draw

-- | Matrix so that y' = -y + h
matrix_invert_y :: Double -> C.Matrix
matrix_invert_y h = C.Matrix 1 0 0 (-1) 0 h

black :: Ca
black = rgba_to_ca (0,0,0,1)

ln_draw :: R -> Ln R -> C.Render ()
ln_draw lw (Ln p q) = Render.line (Ls [p,q]) >> Render.pen lw black ([],0) >> C.stroke

ln_svg_wr :: FilePath -> (Bool,R) -> [Ln R] -> IO ()
ln_svg_wr nm (y_inv,lw) ln_seq = do
  let Wn (Pt x y) (Vc w h) = wn_from_extent (lns_minmax ln_seq)
      r = when y_inv (C.transform (matrix_invert_y h)) >>
          C.translate (- x) (- y) >>
          mapM_ (ln_draw lw) ln_seq
  Render.render_to_file Render.F_Svg (w,h) nm r

-- > let fn = "/home/rohan/uc/invisible/heliotrope/csv/ln-2,3-15-R0.csv"
-- > csv_to_svg_ln (False,0.25) fn "/tmp/L.svg"
csv_to_svg_ln :: (Bool,R) -> FilePath -> FilePath -> IO ()
csv_to_svg_ln opt csv_fn svg_fn = do
  ln_data <- load_ln_data csv_fn
  ln_svg_wr (dropExtension svg_fn) opt ln_data

pt_draw :: R -> Pt R -> C.Render ()
pt_draw rad p = Render.circle p rad >> Render.colour black >> C.fill

pt_svg_wr :: FilePath -> (Bool,R) -> [Pt R] -> IO ()
pt_svg_wr nm (inv_y,sq) pt_seq = do
  let Wn (Pt x y) (Vc w h) = wn_from_extent (ls_minmax (Ls pt_seq))
      r = when inv_y (C.transform (matrix_invert_y h)) >>
          C.translate (- x) (- y) >>
          mapM_ (pt_draw sq) pt_seq
  print (nm,(x,y),(w,h))
  Render.render_to_file Render.F_Svg (w,h) nm r

{-
> let fn = "/home/rohan/uc/invisible/heliotrope/csv/pt-2,3-15-R0.csv"
> csv_to_svg_pt (False,1.5) fn "/tmp/pt.svg"

> let fn = "/home/rohan/uc/invisible/sifting/csv/sifting.pt.csv"
> csv_to_svg_pt (False,0.075) fn "/tmp/pt.svg"

> let fn = "/home/rohan/rd/j/2020-04-19/C1.T2.csv"
> csv_to_svg_pt (False,0.5) fn "/tmp/pt.svg"
-}
csv_to_svg_pt :: (Bool,R) -> FilePath -> FilePath -> IO ()
csv_to_svg_pt opt csv_fn svg_fn = do
  pt_data <- load_pt_data csv_fn
  pt_svg_wr (dropExtension svg_fn) opt pt_data

ls_draw :: R -> (Int,Ls R) -> C.Render ()
ls_draw lw (_,ls) = Render.polygon ls >> Render.pen lw black ([],0) >> C.stroke

ls_svg_wr :: FilePath -> (Bool,R) -> [(Int,Ls R)] -> IO ()
ls_svg_wr nm (inv_y,lw) ls_seq = do
  let Wn (Pt x y) (Vc w h) = wn_from_extent (ls_minmax (ls_concat (map snd ls_seq)))
      r = when inv_y (C.transform (matrix_invert_y h)) >>
          C.translate (- x) (- y) >>
          mapM_ (ls_draw lw) ls_seq
  Render.render_to_file Render.F_Svg (w,h) nm r

-- > csv_to_svg_ls (True,0.25) "/tmp/ne.csv" "/tmp/ne.svg"
csv_to_svg_ls :: (Bool,R) -> FilePath -> FilePath -> IO ()
csv_to_svg_ls opt csv_fn svg_fn = do
  ls_data <- load_ls_data csv_fn
  ls_svg_wr (dropExtension svg_fn) opt ls_data

usage :: [String]
usage =
    ["csv-to-svg ln invert-y:bool line-width:real csv-file svg-file"
    ,"csv-to-svg ls invert-y:bool line-width:real csv-file svg-file"
    ,"csv-to-svg pt invert-y:bool square-radius:real csv-file svg-file"]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["ln",y_inv,lw,csv_fn,svg_fn] -> csv_to_svg_ln (y_inv == "t",read lw) csv_fn svg_fn
    ["ls",y_inv,lw,csv_fn,svg_fn] -> csv_to_svg_ls (y_inv == "t",read lw) csv_fn svg_fn
    ["pt",y_inv,sq,csv_fn,svg_fn] -> csv_to_svg_pt (y_inv == "t",read sq) csv_fn svg_fn
    _ -> putStrLn (unlines usage)
